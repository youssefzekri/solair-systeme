import "./App.scss";
import SolairWrapper from "./components/SolairWrapper/SolairWrapper";
import Sun from "./components/Sun/Sun";
import Earth from "./components/Earth/Earth";
import OtherPlanet from "./components/anotherPlanet/anotherPlanet";
import Stars from "./components/star/Stars";
function App() {
  return (
    <div className="App">
      <Stars/>

      <SolairWrapper
        planet={
          <>
            <Earth />
            <OtherPlanet/>
          </>
        }
        duration={'10s'}
        showBorder
      >
        <Sun />
      </SolairWrapper>
    </div>
  );
}

export default App;
