import Star from "./Star";

export default function Stars() {
  return (
    <div className="Stars">
        <Star/>
        <Star/>
        <Star/>
        <Star/>
        <Star/>
        <Star/>

        <div className="line"></div>
        <div className="line"></div>
    </div>
  )
}
