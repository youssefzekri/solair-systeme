import SolairWrapper from "../SolairWrapper/SolairWrapper";
import earth from "../../assets/earth.png";
import moon from "../../assets/moon.png";
export default function Earth() {
  return (
    <div className="Earth__wrapper">
      <SolairWrapper
        planet={
          <img
            src={moon}
            alt=""
            className="SolairWrapper__chield"
            style={{ width: "2.5rem", top: "-1rem", boxShadow: "0 0 15px white" }}
          />
        }
        duration={'5s'}
        showBorder

      >
        <div className="Earth"></div>
      </SolairWrapper>
    </div>
  );
}
