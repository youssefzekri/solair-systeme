export default function SolairWrapper({ children, planet , duration  , showBorder}) {
  return (
    <div className={`SolairWrapper ${showBorder ? ' --outlined': ''}`} style={{animationDuration : duration}}>
      {planet}
   
      {children}
    </div>
  );
}
