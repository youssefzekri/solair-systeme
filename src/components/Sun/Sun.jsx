export default function Sun() {
  return (
    <div className="Sun">
      <span className="Sun__circle"></span>
    </div>
  )
}
