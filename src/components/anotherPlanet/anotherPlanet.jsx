import other from "../../assets/planet3.png";
export default function OtherPlanet() {
  return (
    <div className="Other">
      <div className="Other__planet">
        <img src={other} alt="" />
      </div>
    </div>
  );
}
